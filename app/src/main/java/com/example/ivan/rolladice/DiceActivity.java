package com.example.ivan.rolladice;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class DiceActivity extends AppCompatActivity implements SensorEventListener{

    Button bRoll;
    ImageView ivDice;
    Random r;
    int rolledNumber;
    private SensorManager sensorManager;
    Sensor accelerometer;
    float prevXValue = 0, prevYValue = 0, prevZValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dice);

        bRoll = (Button) findViewById(R.id.b_roll);
        ivDice = (ImageView) findViewById(R.id.iv_dice);
        r = new Random();

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        sensorManager.registerListener(DiceActivity.this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        bRoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Animation anim = AnimationUtils.loadAnimation(DiceActivity.this, R.anim.shake_fwr);

                final Animation.AnimationListener animationListener = new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        setImageRes();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                };

                showToast();
                anim.setAnimationListener(animationListener);
                ivDice.startAnimation(anim);

            }
        });

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void setImageRes(){
        rolledNumber = r.nextInt(6) + 1;
        int res = getResources().getIdentifier("dice" + rolledNumber, "drawable", getPackageName());
        ivDice.setImageResource(res);
        Log.d("rolledNumber", "" + rolledNumber);
        Log.d("res", "" + res);
    }

    public void showToast(){
        Toast ToastMessage = Toast.makeText(DiceActivity.this, "Rolled a dice", Toast.LENGTH_SHORT);
        //View toastView = ToastMessage.getView();
        //toastView.setBackgroundColor(Color.argb(255, r.nextInt(256), r.nextInt(256), r.nextInt(256)));
        ToastMessage.show();
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float XValue = sensorEvent.values[0];
        float YValue = sensorEvent.values[1];
        float ZValue = sensorEvent.values[2];

        Log.d("DiceActivity", "X: " + Math.abs(prevXValue - XValue) + " Y:" +
                Math.abs(prevYValue - YValue) + " Z:" + Math.abs(prevZValue - ZValue));
        if(Math.abs(prevXValue - XValue) > 10  || Math.abs(prevYValue - YValue) > 10 || Math.abs(prevZValue - ZValue) > 10) {
            final Animation anim = AnimationUtils.loadAnimation(DiceActivity.this, R.anim.shake_fwr);

            final Animation.AnimationListener animationListener = new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    rolledNumber = r.nextInt(6) + 1;
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    showToast();
                    int res = getResources().getIdentifier("dice" + rolledNumber, "drawable", getPackageName());
                    ivDice.setImageResource(res);

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            };


            anim.setAnimationListener(animationListener);
            ivDice.startAnimation(anim);
        }

        prevXValue = XValue;
        prevYValue = YValue;
        prevZValue = ZValue;
    }
}
